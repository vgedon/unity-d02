using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour
{
	private	Vector2			dest;
	private	Rigidbody2D		body;
	private	Animator		animator;
	private GameObject		target;
	private	AudioSource		sound_source;
	private	string			state = "Idle";
	private	Sprite			deathSprite;

	public	AudioClip[]		aknowledge_sounds;
	public	AudioClip[]		select_sounds;
	public	AudioClip[]		help_sounds;
	public	int				HP = 60;
	public	int				DmgMin = 2;
	public	int				DmgMax = 9;
	public	int				Armor = 2;
	
	
	// Use this for initialization
	void Start ()
	{
		body = GetComponent<Rigidbody2D> ();
		dest = RoundVector (body.position);
		animator = GetComponent<Animator> ();
		sound_source = GetComponent<AudioSource> ();
		animator.SetFloat ("x", 0);
		animator.SetFloat ("y", -1);
		deathSprite = GetComponent<SpriteRenderer> ().sprite;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!body.velocity.Equals (Vector2.zero) 
			&& body.IsTouchingLayers (LayerMask.GetMask ("Interactable"))) {
			if (!target || (target && !body.IsTouching (target.GetComponent<CircleCollider2D> ()))) {
				Collider2D[] touch = Physics2D.OverlapCircleAll (
				transform.position,
				GetComponent<CircleCollider2D> ().radius + 0.01f,
				LayerMask.GetMask ("Interactable")
				);
				foreach (Collider2D coll in touch) {
					if ((RoundVector (coll.attachedRigidbody.position)).Equals (dest)) {
						body.velocity = Vector2.zero;
						dest = RoundVector (body.position);
					}
				}
			}
		}
	}

	void FixedUpdate ()
	{
		if (!(RoundVector (body.position)).Equals (dest)) {
			body.AddForce (RoundVector (body.GetPoint (dest).normalized) * 100f);
		}
	}

	void LateUpdate ()
	{
		if (!RoundVector (body.position).Equals (dest) && state != "Walk") {
			if (!body.GetPoint (dest).Equals (Vector2.zero)) {
				animator.SetFloat ("x", Mathf.Round (body.GetPoint (dest).normalized.x));
				animator.SetFloat ("y", Mathf.Round (body.GetPoint (dest).normalized.y));
				animator.SetTrigger ("Walk");
				state = "Walk";
			}
		} else if (RoundVector (body.position).Equals (dest) && state == "Walk") {
			animator.SetTrigger ("Idle");
			state = "Idle";
		}
	}
	void OnCollisionEnter2D (Collision2D coll)
	{
		if (animator && coll.collider.tag != tag && state != "Attack") {
			target = coll.gameObject;
			body.velocity = Vector2.zero;
			dest = RoundVector (body.position);
			animator.SetTrigger ("Attack");
			state = "Attack";
			animator.SetFloat ("x", Mathf.Round (body.GetPoint (coll.rigidbody.position).normalized.x));
			animator.SetFloat ("y", Mathf.Round (body.GetPoint (coll.rigidbody.position).normalized.y));
		}
	}

	void OnCollisionStay2D (Collision2D coll)
	{
		if (animator && coll.collider.tag != tag && state != "Attack") {
			animator.SetTrigger ("Attack");
			animator.SetFloat ("x", Mathf.Round (body.GetPoint (coll.rigidbody.position).normalized.x));
			animator.SetFloat ("y", Mathf.Round (body.GetPoint (coll.rigidbody.position).normalized.y));
		} else if (state == "Attack") {
			state = "Idle";
			animator.SetTrigger ("Idle");
		}
	}

	void OnCollisionExit2D (Collision2D coll)
	{
		if (animator) {
			animator.SetTrigger ("Idle");
			state = "Idle";
			target = null;
		}
	}

	Vector2 RoundVector (Vector2 v)
	{
		v.Set (Mathf.Round (v.x * 2) / 2, Mathf.Round (v.y * 2) / 2);
		return (v);
	}

	public void SetTarget (GameObject destination)
	{
		target = destination;
		dest = RoundVector (target.transform.position);
		if (aknowledge_sounds.Length > 0) {
			sound_source.PlayOneShot (aknowledge_sounds [Random.Range (0, aknowledge_sounds.Length)]);
		}
	}

	public void SetDestination (Vector2 destination)
	{
		dest = RoundVector (destination);
		target = null;
		if (aknowledge_sounds.Length > 0) {
			sound_source.PlayOneShot (aknowledge_sounds [Random.Range (0, aknowledge_sounds.Length)]);
		}
	}

	public void Selected ()
	{
		sound_source.PlayOneShot (select_sounds [Random.Range (0, select_sounds.Length)]);
	}

	public void Attack ()
	{
		if (target && target.GetComponent<MovingObject> ().IsAlive ()) {
			target.GetComponent<MovingObject> ().TakeDamage (Random.Range (DmgMin, DmgMax));
			if (!target.GetComponent<MovingObject> ().IsAlive ()) {
				target = null;
				animator.SetTrigger ("Idle");
				state = "Idle";
			}
		} else if (animator) {
			animator.SetTrigger ("Idle");
			state = "Idle";
			target = null;
		}
	}

	public void TakeDamage (int dmg)
	{
		HP -= dmg - Armor;
		if (HP <= 0) {
			animator.SetTrigger ("Dead");
			state = "Dead";
		}
	}

	public void SetDeadBody ()
	{
		GetComponent<SpriteRenderer> ().sprite = deathSprite;
		GetComponent<CircleCollider2D> ().enabled = false;
	}

	public bool IsAlive ()
	{
		return (HP > 0);
	}

}



