﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseManager : MonoBehaviour
{
	List<MovingObject>	selection = new List<MovingObject> ();


	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit2D ray = Physics2D.GetRayIntersection (Camera.main.ScreenPointToRay (Input.mousePosition));
			MovingObject hit = ray.collider.GetComponent<MovingObject> ();
			switch (ray.collider.tag) {
			case "Ally":
				if (!selection.Contains (hit)) {
					if (!Input.GetKey (KeyCode.LeftControl)) {
						selection.Clear ();
					}
					selection.Add (hit);
					hit.Selected ();
				}
				break;
			case "Walkable":
				selection.ForEach (
					delegate(MovingObject obj) {
					obj.SetDestination (Camera.main.ScreenToWorldPoint (Input.mousePosition));
				});
				break;
			case "Ennemy":
				selection.ForEach (delegate(MovingObject obj) {
					obj.SetTarget (ray.collider.gameObject);
				});
				break;
			default:
				break;
			}
		} else if (Input.GetMouseButtonDown (1)) {
			selection.Clear ();
		}
	}
}
